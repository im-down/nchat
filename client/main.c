#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/socket.h>

#include <netinet/in.h>

int main() {
	
	// create socket
	int net_socket;
	net_socket = socket(AF_INET, SOCK_STREAM, 0);

	// specify address for socket
	struct sockaddr_in server_address;
	server_address.sin_family = AF_INET;
	server_address.sin_port = htons(7140);
	server_address.sin_addr.s_addr = INADDR_ANY;

	int connection_status = connect(net_socket, (struct sockaddr *) &server_address, sizeof(server_address) );
	// check for errors with connection
	if (connection_status == -1) {
		printf("There was an error connecting to the remote socket\n\n");
	}

	// recive data from server
	char server_response[256];
	recv(net_socket, &server_response, sizeof(server_response), 0);
	
	//print data to screen
	printf("%s\n", server_response);
	
	//close socket
	close(socket);
	return 0;
}
