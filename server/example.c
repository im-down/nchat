#include <stdio.h>
#include <stdlib.h>

#include <sys/socket.h>
#include <sys/types.h>

#include <netinet/in.h>

int main() {
	
	char server_message[256] = "they wanna see me gone because of my hops";

	// create server socket
	printf("creating socket...\n");
	int server_socket;
	server_socket = socket(AF_INET, SOCK_STREAM, 0);

	// define server addr
	struct sockaddr_in server_address;
	server_address.sin_family = AF_INET;
	server_address.sin_port = htons(7140);
	server_address.sin_addr.s_addr= INADDR_ANY;

	// bind socket to ip address
	printf("binding ip address to port 1488...\n");
	bind(server_socket, (struct sockaddr*) &server_address, sizeof(server_address));

	// listen to connections
	printf("listening for connections...\n");
	listen(server_socket, 10);

	int client_socket;
	client_socket = accept(server_socket, NULL, NULL);
	
	// send the message to client
	printf("client found. sending message...\n");
	send(client_socket, server_message, sizeof(server_message), 0);

	// close socket
	printf("message sent. closing socket\n");
	close(server_socket);
	return 0;
}
